<?php
App::uses('AppModel', 'Model');
/**
 * FlightPrice Model
 *
 * @property Flight $Flight
 * @property Class $Class
 */
class FlightPrice extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

	/**
	 * belongsTo associations
	 *
	 * @var array
	 */
	public $belongsTo = array(
		'Flight' => array(
			'className' => 'Flight',
			'foreignKey' => 'flight_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Class' => array(
			'className' => 'Classe',
			'foreignKey' => 'class_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
