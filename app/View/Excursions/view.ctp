<div class="row-fluid"><div class="span12">

	<div class="row-fluid header">
		<div class="span12">
			<h2><?php  echo __('Excursion'); ?></h2>

			<div class="btn-group pull-right">
				<button class="btn" type="button">
					<?php
						echo $this->Form->postLink($this->Html->image('fugue-icons/cross-button.png',
								array('class' => 'fugue-icon fugue-icon-push-right', 'alt' => __('Delete'))
							) . __('Delete'),
							array('controller' => 'excursions', 'action' => 'delete', 'manager' => true, $excursion['Excursion']['id']),
							array('escape' => false),
							__('Are you sure you want to delete # %s?', $excursion['Excursion']['id'])
						);
					?>
				</button>
			</div>

			<div class="btn-group pull-left">
				<button class="btn" type="button">
					<?php
						echo $this->Html->link($this->Html->image('fugue-icons/plus-button.png',
								array('class' => 'fugue-icon fugue-icon-push-right', 'alt' => __('Add'))
							) . __('Add'),
							array('controller' => 'excursions', 'action' => 'add'),
							array('escape' => false)
						);
					?>
				</button>
				<button class="btn" type="button">
					<?php
						echo $this->Html->link($this->Html->image('fugue-icons/document--pencil.png',
								array('class' => 'fugue-icon fugue-icon-push-right', 'alt' => __('Edit'))
							) . __('Edit'),
							array('controller' => 'excursions', 'action' => 'edit', $excursion['Excursion']['id']),
							array('escape' => false)
						);
					?>
				</button>
				<button class="btn" type="button">
					<?php
						echo $this->Html->link($this->Html->image('fugue-icons/documents-stack.png',
								array('class' => 'fugue-icon fugue-icon-push-right', 'alt' => __('Index'))
							) . __('Index'),
							array('controller' => 'excursions', 'action' => 'index', 'manager' => true),
							array('escape' => false)
						);
					?>
				</button>
			</div>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span12">
			<ul class="breadcrumb">
				<li><?php echo $this->Html->link(__('Dashboard'),array('controller' => 'dashboard', 'action' => 'index', 'manager' => true));?> <span class="divider">/</span></li>
				<li><?php echo $this->Html->link(__('Excursions'), array('controller' => 'excursions', 'action' => 'index', 'manager' => true)); ?> <span class="divider">/</span></li>
				<li class="active"><?php echo __('View # ') . $excursion['Excursion']['id']; ?></li>
			</ul>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span8">
			<h3><?php  echo __('Excursion'); ?></h3>
			<dl>
				<dt><?php echo __('Id'); ?></dt>
				<dd>
					<?php echo h($excursion['Excursion']['id']); ?>
				</dd>
				<dt><?php echo __('Name'); ?></dt>
				<dd>
					<?php echo h($excursion['Excursion']['name']); ?>
				</dd>
				<dt><?php echo __('Price'); ?></dt>
				<dd>
					<?php echo h($excursion['Excursion']['price']); ?>
				</dd>
			</dl>
		</div>
		<div class="span4 actions">
			<h3><?php echo __('Actions'); ?></h3>
			<ul class="nav nav-pills nav-stacked">
				<li><?php echo $this->Html->link(__('Edit Excursion'), array('controller' => 'excursions', 'action' => 'edit', $excursion['Excursion']['id'])); ?> </li>
				<li><?php echo $this->Form->postLink(__('Delete Excursion'), array('controller' => 'excursions', 'action' => 'delete', $excursion['Excursion']['id']), null, __('Are you sure you want to delete # %s?', $excursion['Excursion']['id'])); ?> </li>
				<li><?php echo $this->Html->link(__('List Excursions'), array('controller' => 'excursions', 'action' => 'index')); ?> </li>
				<li><?php echo $this->Html->link(__('New Excursion'), array('controller' => 'excursions', 'action' => 'add')); ?> </li>
			</ul>
		</div>
	</div>

</div></div>
