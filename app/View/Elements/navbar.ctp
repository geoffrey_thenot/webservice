<div class="navbar navbar-static-top navbar-inverse">
    <div class="navbar-inner">
        <div class="container">
            <?php
                echo $this->Html->link(
                    __('Voyages PER'),
                    array(
                        'plugin'        => null,
                        'controller'    => 'dashboard',
                        'action'        => 'index',
                    ),
                    array('class' => 'brand')
                );
            ?>
            <ul class="nav">
                <li class="dropdown">
                        <li><?php
                            echo $this->Html->link(
                                __('Hotels'),
                                array(
                                    'plugin'        => null,
                                    'controller'    => 'hotels',
                                    'action'        => 'index'
                                )
                            );
                        ?></li>
                        <li><?php
                            echo $this->Html->link(
                                __('Rooms'),
                                array(
                                    'plugin'        => null,
                                    'controller'    => 'rooms',
                                    'action'        => 'index'
                                )
                            );
                        ?></li>
                        <li><?php
                            echo $this->Html->link(
                                __('Countries'),
                                array(
                                    'plugin'        => null,
                                    'controller'    => 'countries',
                                    'action'        => 'index'
                                )
                            );
                        ?></li>
                        <li><?php
                            echo $this->Html->link(
                                __('Cities'),
                                array(
                                    'plugin'        => null,
                                    'controller'    => 'cities',
                                    'action'        => 'index'
                                )
                            );
                        ?></li>
                        <li><?php
                            echo $this->Html->link(
                                __('Flights'),
                                array(
                                    'plugin'        => null,
                                    'controller'    => 'flights',
                                    'action'        => 'index'
                                )
                            );
                        ?></li>
                        <li><?php
                            echo $this->Html->link(
                                __('Flight prices'),
                                array(
                                    'plugin'        => null,
                                    'controller'    => 'flightPrices',
                                    'action'        => 'index'
                                )
                            );
                        ?></li>

                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
