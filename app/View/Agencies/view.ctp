<div class="row-fluid"><div class="span12">

	<div class="row-fluid header">
		<div class="span12">
			<h2><?php  echo __('Agency'); ?></h2>

			<div class="btn-group pull-right">
				<button class="btn" type="button">
					<?php
						echo $this->Form->postLink($this->Html->image('fugue-icons/cross-button.png',
								array('class' => 'fugue-icon fugue-icon-push-right', 'alt' => __('Delete'))
							) . __('Delete'),
							array('controller' => 'agencies', 'action' => 'delete', 'manager' => true, $agency['Agency']['id']),
							array('escape' => false),
							__('Are you sure you want to delete # %s?', $agency['Agency']['id'])
						);
					?>
				</button>
			</div>

			<div class="btn-group pull-left">
				<button class="btn" type="button">
					<?php
						echo $this->Html->link($this->Html->image('fugue-icons/plus-button.png',
								array('class' => 'fugue-icon fugue-icon-push-right', 'alt' => __('Add'))
							) . __('Add'),
							array('controller' => 'agencies', 'action' => 'add'),
							array('escape' => false)
						);
					?>
				</button>
				<button class="btn" type="button">
					<?php
						echo $this->Html->link($this->Html->image('fugue-icons/document--pencil.png',
								array('class' => 'fugue-icon fugue-icon-push-right', 'alt' => __('Edit'))
							) . __('Edit'),
							array('controller' => 'agencies', 'action' => 'edit', $agency['Agency']['id']),
							array('escape' => false)
						);
					?>
				</button>
				<button class="btn" type="button">
					<?php
						echo $this->Html->link($this->Html->image('fugue-icons/documents-stack.png',
								array('class' => 'fugue-icon fugue-icon-push-right', 'alt' => __('Index'))
							) . __('Index'),
							array('controller' => 'agencies', 'action' => 'index', 'manager' => true),
							array('escape' => false)
						);
					?>
				</button>
			</div>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span12">
			<ul class="breadcrumb">
				<li><?php echo $this->Html->link(__('Dashboard'),array('controller' => 'dashboard', 'action' => 'index', 'manager' => true));?> <span class="divider">/</span></li>
				<li><?php echo $this->Html->link(__('Agencies'), array('controller' => 'agencies', 'action' => 'index', 'manager' => true)); ?> <span class="divider">/</span></li>
				<li class="active"><?php echo __('View # ') . $agency['Agency']['id']; ?></li>
			</ul>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span8">
			<h3><?php  echo __('Agency'); ?></h3>
			<dl>
				<dt><?php echo __('Id'); ?></dt>
				<dd>
					<?php echo h($agency['Agency']['id']); ?>
				</dd>
				<dt><?php echo __('City'); ?></dt>
				<dd>
					<?php echo $this->Html->link($agency['City']['name'], array('controller' => 'cities', 'action' => 'view', $agency['City']['id'])); ?>
				</dd>
				<dt><?php echo __('Name'); ?></dt>
				<dd>
					<?php echo h($agency['Agency']['name']); ?>
				</dd>
				<dt><?php echo __('Address'); ?></dt>
				<dd>
					<?php echo h($agency['Agency']['address']); ?>
				</dd>
				<dt><?php echo __('Zip Code'); ?></dt>
				<dd>
					<?php echo h($agency['Agency']['zip_code']); ?>
				</dd>
				<dt><?php echo __('Website'); ?></dt>
				<dd>
					<?php echo h($agency['Agency']['website']); ?>
				</dd>
			</dl>
		</div>
		<div class="span4 actions">
			<h3><?php echo __('Actions'); ?></h3>
			<ul class="nav nav-pills nav-stacked">
				<li><?php echo $this->Html->link(__('Edit Agency'), array('controller' => 'agencies', 'action' => 'edit', $agency['Agency']['id'])); ?> </li>
				<li><?php echo $this->Form->postLink(__('Delete Agency'), array('controller' => 'agencies', 'action' => 'delete', $agency['Agency']['id']), null, __('Are you sure you want to delete # %s?', $agency['Agency']['id'])); ?> </li>
				<li><?php echo $this->Html->link(__('List Agencies'), array('controller' => 'agencies', 'action' => 'index')); ?> </li>
				<li><?php echo $this->Html->link(__('New Agency'), array('controller' => 'agencies', 'action' => 'add')); ?> </li>
				<li><?php echo $this->Html->link(__('List Cities'), array('controller' => 'cities', 'action' => 'index')); ?> </li>
				<li><?php echo $this->Html->link(__('New City'), array('controller' => 'cities', 'action' => 'add')); ?> </li>
			</ul>
		</div>
	</div>

</div></div>
