<div class="row-fluid"><div class="span12">

	<div class="row-fluid header">
		<div class="span12">
			<h2><?php  echo __('Flight Price'); ?></h2>

			<div class="btn-group pull-right">
				<button class="btn" type="button">
					<?php
						echo $this->Form->postLink($this->Html->image('fugue-icons/cross-button.png',
								array('class' => 'fugue-icon fugue-icon-push-right', 'alt' => __('Delete'))
							) . __('Delete'),
							array('controller' => 'flightPrices', 'action' => 'delete', 'manager' => true, $flightPrice['FlightPrice']['id']),
							array('escape' => false),
							__('Are you sure you want to delete # %s?', $flightPrice['FlightPrice']['id'])
						);
					?>
				</button>
			</div>

			<div class="btn-group pull-left">
				<button class="btn" type="button">
					<?php
						echo $this->Html->link($this->Html->image('fugue-icons/plus-button.png',
								array('class' => 'fugue-icon fugue-icon-push-right', 'alt' => __('Add'))
							) . __('Add'),
							array('controller' => 'flightPrices', 'action' => 'add'),
							array('escape' => false)
						);
					?>
				</button>
				<button class="btn" type="button">
					<?php
						echo $this->Html->link($this->Html->image('fugue-icons/document--pencil.png',
								array('class' => 'fugue-icon fugue-icon-push-right', 'alt' => __('Edit'))
							) . __('Edit'),
							array('controller' => 'flightPrices', 'action' => 'edit', $flightPrice['FlightPrice']['id']),
							array('escape' => false)
						);
					?>
				</button>
				<button class="btn" type="button">
					<?php
						echo $this->Html->link($this->Html->image('fugue-icons/documents-stack.png',
								array('class' => 'fugue-icon fugue-icon-push-right', 'alt' => __('Index'))
							) . __('Index'),
							array('controller' => 'flightPrices', 'action' => 'index', 'manager' => true),
							array('escape' => false)
						);
					?>
				</button>
			</div>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span12">
			<ul class="breadcrumb">
				<li><?php echo $this->Html->link(__('Dashboard'),array('controller' => 'dashboard', 'action' => 'index', 'manager' => true));?> <span class="divider">/</span></li>
				<li><?php echo $this->Html->link(__('Flight Prices'), array('controller' => 'flightPrices', 'action' => 'index', 'manager' => true)); ?> <span class="divider">/</span></li>
				<li class="active"><?php echo __('View # ') . $flightPrice['FlightPrice']['id']; ?></li>
			</ul>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span8">
			<h3><?php  echo __('Flight Price'); ?></h3>
			<dl>
				<dt><?php echo __('Id'); ?></dt>
				<dd>
					<?php echo h($flightPrice['FlightPrice']['id']); ?>
				</dd>
				<dt><?php echo __('Flight'); ?></dt>
				<dd>
					<?php echo $this->Html->link($flightPrice['Flight']['id'], array('controller' => 'flights', 'action' => 'view', $flightPrice['Flight']['id'])); ?>
				</dd>
				<dt><?php echo __('Class'); ?></dt>
				<dd>
					<?php echo $this->Html->link($flightPrice['Class']['name'], array('controller' => 'classes', 'action' => 'view', $flightPrice['Class']['id'])); ?>
				</dd>
				<dt><?php echo __('Price'); ?></dt>
				<dd>
					<?php echo h($flightPrice['FlightPrice']['price']); ?>
				</dd>
			</dl>
		</div>
		<div class="span4 actions">
			<h3><?php echo __('Actions'); ?></h3>
			<ul class="nav nav-pills nav-stacked">
				<li><?php echo $this->Html->link(__('Edit Flight Price'), array('controller' => 'flightPrices', 'action' => 'edit', $flightPrice['FlightPrice']['id'])); ?> </li>
				<li><?php echo $this->Form->postLink(__('Delete Flight Price'), array('controller' => 'flightPrices', 'action' => 'delete', $flightPrice['FlightPrice']['id']), null, __('Are you sure you want to delete # %s?', $flightPrice['FlightPrice']['id'])); ?> </li>
				<li><?php echo $this->Html->link(__('List Flight Prices'), array('controller' => 'flightPrices', 'action' => 'index')); ?> </li>
				<li><?php echo $this->Html->link(__('New Flight Price'), array('controller' => 'flightPrices', 'action' => 'add')); ?> </li>
				<li><?php echo $this->Html->link(__('List Flights'), array('controller' => 'flights', 'action' => 'index')); ?> </li>
				<li><?php echo $this->Html->link(__('New Flight'), array('controller' => 'flights', 'action' => 'add')); ?> </li>
				<li><?php echo $this->Html->link(__('List Classes'), array('controller' => 'classes', 'action' => 'index')); ?> </li>
				<li><?php echo $this->Html->link(__('New Class'), array('controller' => 'classes', 'action' => 'add')); ?> </li>
			</ul>
		</div>
	</div>

</div></div>
