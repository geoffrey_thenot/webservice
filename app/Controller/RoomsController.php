<?php
App::uses('AppController', 'Controller');
/**
 * Rooms Controller
 *
 * @property Room $Room
 */
class RoomsController extends AppController
{

	/**
	 * index method
	 *
	 * @return void
	 */
	public function index()
	{
		$this->Room->recursive = 0;
		$rooms = $this->paginate();
		$this->set(compact('rooms'));
	}

	/**
	 * view method
	 *
	 * @param string $id
	 * @return void
	 */
	public function view($id = null)
	{
		$this->Room->id = $id;
		if (!$this->Room->exists()) {
		//	$this->Session->setFlash(__('Invalid room.'), 'Default/Flash/error');
			$this->redirect(array('action' => 'index'));
		}
		$room = $this->Room->read(null, $id);
		$this->set(compact('room'));
	}

	/**
	 * add method
	 *
	 * @return void
	 */
	public function add()
	{
		if ($this->request->is('post')) {
			$this->Room->create();
			if ($this->Room->save($this->request->data)) {
			//	$this->Session->setFlash(__('The room has been saved'), 'Default/Flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
			//	$this->Session->setFlash(__('The room could not be saved. Please, try again.', 'Default/Flash/error'));
				$this->redirect($this->referer());
			}
		}
		$hotels = $this->Room->Hotel->find('list');
		$this->set(compact('hotels'));
	}

	/**
	 * edit method
	 *
	 * @param string $id
	 * @return void
	 */
	public function edit($id = null)
	{
		$this->Room->id = $id;
		if (!$this->Room->exists()) {
			//$this->Session->setFlash(__('Invalid room.'), 'Default/Flash/error');
			$this->redirect(array('action' => 'index'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Room->save($this->request->data)) {
			//	$this->Session->setFlash(__('The room has been updated.'), 'Default/Flash/information');
				$this->redirect(array('action' => 'index'));
			} else {
			//	$this->Session->setFlash(__('The room could not be saved. Please, try again.', 'Default/Flash/error'));
				$this->redirect($this->referer());
			}
		} else {
			$this->request->data = $this->Room->read(null, $id);
		}
		$hotels = $this->Room->Hotel->find('list');
		$this->set(compact('hotels'));
	}

	/**
	 * delete method
	 *
	 * @throws MethodNotAllowedException
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null)
	{
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Room->id = $id;
		if (!$this->Room->exists()) {
		//	$this->Session->setFlash(__('Invalid Room'), 'Default/Flash/error');
			$this->redirect(array('action' => 'index'));
		}
		if ($this->Room->delete()) {
		//	$this->Session->setFlash(__('Room deleted'), 'Default/Flash/default');
			$this->redirect(array('action' => 'index'));
		}
		//$this->Session->setFlash(__('Room was not deleted'), 'Default/Flash/error');
		$this->redirect(array('action' => 'index'));
	}

	public function listing(){
		$conditions = array();
		if (!empty($this->request['url']['hotel'])) {
			$conditions['Hotel.name'] = $this->request['url']['hotel'];
		}
		//exit(var_dump($conditions));
		$rooms = $this->Room->find("all", array(
											'conditions' => $conditions));

		$_rooms = array('rooms' => null);
        foreach ($rooms as $room) {
                $_rooms['rooms']['room'][] = $room['Room'];
        }
        $xml = Xml::fromArray($_rooms);
        $this->set(compact('xml')); 
	}
}
