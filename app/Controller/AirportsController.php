<?php
App::uses('AppController', 'Controller');
/**
 * Airports Controller
 *
 * @property Airport $Airport
 */
class AirportsController extends AppController
{

	/**
	 * index method
	 *
	 * @return void
	 */
	public function index()
	{
		$this->Airport->recursive = 0;
		$airports = $this->paginate();
		$this->set(compact('airports'));
	}

	/**
	 * view method
	 *
	 * @param string $id
	 * @return void
	 */
	public function view($id = null)
	{
		$this->Airport->id = $id;
		if (!$this->Airport->exists()) {
			//$this->Session->setFlash(__('Invalid airport.'), 'Default/Flash/error');
			$this->redirect(array('action' => 'index'));
		}
		$airport = $this->Airport->read(null, $id);
		$this->set(compact('airport'));
	}

	/**
	 * add method
	 *
	 * @return void
	 */
	public function add()
	{
		if ($this->request->is('post')) {
			$this->Airport->create();
			if ($this->Airport->save($this->request->data)) {
				//$this->Session->setFlash(__('The airport has been saved'), 'Default/Flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				//$this->Session->setFlash(__('The airport could not be saved. Please, try again.', 'Default/Flash/error'));
				$this->redirect($this->referer());
			}
		}
		$cities = $this->Airport->City->find('list');
		$this->set(compact('cities'));
	}

	/**
	 * edit method
	 *
	 * @param string $id
	 * @return void
	 */
	public function edit($id = null)
	{
		$this->Airport->id = $id;
		if (!$this->Airport->exists()) {
			//$this->Session->setFlash(__('Invalid airport.'), 'Default/Flash/error');
			$this->redirect(array('action' => 'index'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Airport->save($this->request->data)) {
				//$this->Session->setFlash(__('The airport has been updated.'), 'Default/Flash/information');
				$this->redirect(array('action' => 'index'));
			} else {
				//$this->Session->setFlash(__('The airport could not be saved. Please, try again.', 'Default/Flash/error'));
				$this->redirect($this->referer());
			}
		} else {
			$this->request->data = $this->Airport->read(null, $id);
		}
		$cities = $this->Airport->City->find('list');
		$this->set(compact('cities'));
	}

	/**
	 * delete method
	 *
	 * @throws MethodNotAllowedException
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null)
	{
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Airport->id = $id;
		if (!$this->Airport->exists()) {
			//$this->Session->setFlash(__('Invalid Airport'), 'Default/Flash/error');
			$this->redirect(array('action' => 'index'));
		}
		if ($this->Airport->delete()) {
			//$this->Session->setFlash(__('Airport deleted'), 'Default/Flash/default');
			$this->redirect(array('action' => 'index'));
		}
		//$this->Session->setFlash(__('Airport was not deleted'), 'Default/Flash/error');
		$this->redirect(array('action' => 'index'));
	}

	public function listing(){
		$conditions = array();
		if (!empty($this->request['url']['city'])) {
			$conditions['City.name'] = $this->request['url']['city'];
		}
		//exit(var_dump($conditions));
		$airports = $this->Airport->find("all", array(
											'conditions' => $conditions));

		$_airports = array('airports' => null);
        foreach ($airports as $airport) {
                $_airports['airports']['airport'][] = $airport['Airport'];
        }
        $xml = Xml::fromArray($_airports);
        $this->set(compact('xml')); 
	}
}
