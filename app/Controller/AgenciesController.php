<?php
App::uses('AppController', 'Controller');
/**
 * Agencies Controller
 *
 * @property Agency $Agency
 */
class AgenciesController extends AppController
{

	/**
	 * index method
	 *
	 * @return void
	 */
	public function index()
	{
		$this->Agency->recursive = 0;
		$agencies = $this->paginate();
		$this->set(compact('agencies'));
	}

	/**
	 * view method
	 *
	 * @param string $id
	 * @return void
	 */
	public function view($id = null)
	{
		$this->Agency->id = $id;
		if (!$this->Agency->exists()) {
			//$this->Session->setFlash(__('Invalid agency.'), 'Default/Flash/error');
			$this->redirect(array('action' => 'index'));
		}
		$agency = $this->Agency->read(null, $id);
		$this->set(compact('agency'));
	}

	/**
	 * add method
	 *
	 * @return void
	 */
	public function add()
	{
		if ($this->request->is('post')) {
			$this->Agency->create();
			if ($this->Agency->save($this->request->data)) {
				//$this->Session->setFlash(__('The agency has been saved'), 'Default/Flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				//$this->Session->setFlash(__('The agency could not be saved. Please, try again.', 'Default/Flash/error'));
				$this->redirect($this->referer());
			}
		}
		$cities = $this->Agency->City->find('list');
		$this->set(compact('cities'));
	}

	/**
	 * edit method
	 *
	 * @param string $id
	 * @return void
	 */
	public function edit($id = null)
	{
		$this->Agency->id = $id;
		if (!$this->Agency->exists()) {
			//$this->Session->setFlash(__('Invalid agency.'), 'Default/Flash/error');
			$this->redirect(array('action' => 'index'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Agency->save($this->request->data)) {
				//$this->Session->setFlash(__('The agency has been updated.'), 'Default/Flash/information');
				$this->redirect(array('action' => 'index'));
			} else {
				//$this->Session->setFlash(__('The agency could not be saved. Please, try again.', 'Default/Flash/error'));
				$this->redirect($this->referer());
			}
		} else {
			$this->request->data = $this->Agency->read(null, $id);
		}
		$cities = $this->Agency->City->find('list');
		$this->set(compact('cities'));
	}

	/**
	 * delete method
	 *
	 * @throws MethodNotAllowedException
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null)
	{
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Agency->id = $id;
		if (!$this->Agency->exists()) {
			//$this->Session->setFlash(__('Invalid Agency'), 'Default/Flash/error');
			$this->redirect(array('action' => 'index'));
		}
		if ($this->Agency->delete()) {
			$this->Session->setFlash(__('Agency deleted'), 'Default/Flash/default');
			$this->redirect(array('action' => 'index'));
		}
		//$this->Session->setFlash(__('Agency was not deleted'), 'Default/Flash/error');
		$this->redirect(array('action' => 'index'));
	}

	public function listing(){
		$conditions = array();
		if (!empty($this->request['url']['city'])) {
			$conditions['City.name'] = $this->request['url']['city'];
		}
		//exit(var_dump($conditions));
		$agencies = $this->Agency->find("all", array(
											'conditions' => $conditions));

        $_agencies = array('agencies' => null);
        foreach ($agencies as $agency) {
                $_agencies['agencies']['agency'][] = $agency['Agency'];
        }
        $xml = Xml::fromArray($_agencies);
        $this->set(compact('xml')); 
	}
}
