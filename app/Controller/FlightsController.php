<?php
App::uses('AppController', 'Controller');
/**
 * Flights Controller
 *
 * @property Flight $Flight
 */
class FlightsController extends AppController
{

	/**
	 * index method
	 *
	 * @return void
	 */
	public function index()
	{
		$this->Flight->recursive = 0;
		$flights = $this->paginate();
		$this->set(compact('flights'));
	}

	/**
	 * view method
	 *
	 * @param string $id
	 * @return void
	 */
	public function view($id = null)
	{
		$this->Flight->id = $id;
		if (!$this->Flight->exists()) {
		//	$this->Session->setFlash(__('Invalid flight.'), 'Default/Flash/error');
			$this->redirect(array('action' => 'index'));
		}
		$flight = $this->Flight->read(null, $id);
		$this->set(compact('flight'));
	}

	/**
	 * add method
	 *
	 * @return void
	 */
	public function add()
	{
		if ($this->request->is('post')) {
			$this->Flight->create();
			if ($this->Flight->save($this->request->data)) {
			//	$this->Session->setFlash(__('The flight has been saved'), 'Default/Flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
			//	$this->Session->setFlash(__('The flight could not be saved. Please, try again.', 'Default/Flash/error'));
				$this->redirect($this->referer());
			}
		}
		$departureCities = $this->Flight->DepartureCity->find('list');
		$arrivalCities = $this->Flight->ArrivalCity->find('list');
		$this->set(compact('departureCities', 'arrivalCities'));
	}

	/**
	 * edit method
	 *
	 * @param string $id
	 * @return void
	 */
	public function edit($id = null)
	{
		$this->Flight->id = $id;
		if (!$this->Flight->exists()) {
			//$this->Session->setFlash(__('Invalid flight.'), 'Default/Flash/error');
			$this->redirect(array('action' => 'index'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Flight->save($this->request->data)) {
				//$this->Session->setFlash(__('The flight has been updated.'), 'Default/Flash/information');
				$this->redirect(array('action' => 'index'));
			} else {
				//$this->Session->setFlash(__('The flight could not be saved. Please, try again.', 'Default/Flash/error'));
				$this->redirect($this->referer());
			}
		} else {
			$this->request->data = $this->Flight->read(null, $id);
		}
		$departureCities = $this->Flight->DepartureCity->find('list');
		$arrivalCities = $this->Flight->ArrivalCity->find('list');
		$this->set(compact('departureCities', 'arrivalCities'));
	}

	/**
	 * delete method
	 *
	 * @throws MethodNotAllowedException
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null)
	{
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Flight->id = $id;
		if (!$this->Flight->exists()) {
			//$this->Session->setFlash(__('Invalid Flight'), 'Default/Flash/error');
			$this->redirect(array('action' => 'index'));
		}
		if ($this->Flight->delete()) {
			//$this->Session->setFlash(__('Flight deleted'), 'Default/Flash/default');
			$this->redirect(array('action' => 'index'));
		}
		//$this->Session->setFlash(__('Flight was not deleted'), 'Default/Flash/error');
		$this->redirect(array('action' => 'index'));
	}


	public function listing(){
		//Paramètres
		//Ville de départ - Arrivée
		//Date du départ - Retour
		//Heure du départ - Retour

		$conditions = array();
		if (!empty($this->request['url']['departure'])) {
			$conditions['DepartureCity.name'] = $this->request['url']['departure'];
		}
		if (!empty($this->request['url']['arrival'])) {
			$conditions['ArrivalCity.name'] = $this->request['url']['arrival'];
		}

		$flights = $this->Flight->find("all", array(
											'conditions' => $conditions));


        $_flights = array('flights' => null);
        foreach ($flights as $flight) {
                $_flights['flights']['flight'][] = $flight['Flight'];
        }
        $xml = Xml::fromArray($_flights);
        $this->set(compact('xml')); 
	}
}
