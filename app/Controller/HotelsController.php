<?php
App::uses('AppController', 'Controller');
/**
 * Hotels Controller
 *
 * @property Hotel $Hotel
 */
class HotelsController extends AppController
{

	/**
	 * index method
	 *
	 * @return void
	 */
	public function index()
	{
		$this->Hotel->recursive = 0;
		$hotels = $this->paginate();
		$this->set(compact('hotels'));
	}

	/**
	 * view method
	 *
	 * @param string $id
	 * @return void
	 */
	public function view($id = null)
	{
		$this->Hotel->id = $id;
		if (!$this->Hotel->exists()) {
		//	$this->Session->setFlash(__('Invalid hotel.'), 'Default/Flash/error');
			$this->redirect(array('action' => 'index'));
		}
		$hotel = $this->Hotel->read(null, $id);
		$this->set(compact('hotel'));
	}

	/**
	 * add method
	 *
	 * @return void
	 */
	public function add()
	{
		if ($this->request->is('post')) {
			$this->Hotel->create();
			if ($this->Hotel->save($this->request->data)) {
			//	$this->Session->setFlash(__('The hotel has been saved'), 'Default/Flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
			//	$this->Session->setFlash(__('The hotel could not be saved. Please, try again.', 'Default/Flash/error'));
				$this->redirect($this->referer());
			}
		}
		$cities = $this->Hotel->City->find('list');
		$this->set(compact('cities'));
	}

	/**
	 * edit method
	 *
	 * @param string $id
	 * @return void
	 */
	public function edit($id = null)
	{
		$this->Hotel->id = $id;
		if (!$this->Hotel->exists()) {
			//$this->Session->setFlash(__('Invalid hotel.'), 'Default/Flash/error');
			$this->redirect(array('action' => 'index'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Hotel->save($this->request->data)) {
			//	$this->Session->setFlash(__('The hotel has been updated.'), 'Default/Flash/information');
				$this->redirect(array('action' => 'index'));
			} else {
			//	$this->Session->setFlash(__('The hotel could not be saved. Please, try again.', 'Default/Flash/error'));
				$this->redirect($this->referer());
			}
		} else {
			$this->request->data = $this->Hotel->read(null, $id);
		}
		$cities = $this->Hotel->City->find('list');
		$this->set(compact('cities'));
	}

	/**
	 * delete method
	 *
	 * @throws MethodNotAllowedException
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null)
	{
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Hotel->id = $id;
		if (!$this->Hotel->exists()) {
		//	$this->Session->setFlash(__('Invalid Hotel'), 'Default/Flash/error');
			$this->redirect(array('action' => 'index'));
		}
		if ($this->Hotel->delete()) {
			//$this->Session->setFlash(__('Hotel deleted'), 'Default/Flash/default');
			$this->redirect(array('action' => 'index'));
		}
		//$this->Session->setFlash(__('Hotel was not deleted'), 'Default/Flash/error');
		$this->redirect(array('action' => 'index'));
	}

	public function listing() {
		$conditions = array();
		if (!empty($this->request['url']['country'])) {
			$conditions['Country.name'] = $this->request['url']['country'];
		}
		if (!empty($this->request['url']['city'])) {
			$conditions['City.name'] = $this->request['url']['city'];
		}
		//exit(var_dump($conditions));
		$hotels = $this->Hotel->find("all", array(
											'conditions' => $conditions));

        $_hotels = array('hotels' => null);
        foreach ($hotels as $hotel) {
                $_hotels['hotels']['hotel'][] = $hotel['Hotel'];
        }
        $xml = Xml::fromArray($_hotels);
        $this->set(compact('xml')); 
	}

	public function informations($id = null){
		$this->Hotel->id = $id;

		if (!$this->Hotel->exists()) {
			$this->Session->setFlash(__('Invalid hotel.'), 'Default/Flash/error');
			$this->redirect(array('action' => 'index'));
		}
		$hotel = $this->Hotel->read(null, $id);

		$hotel['Hotel']['free_rooms'] = $this->getFreeRooms();

		$xml = Xml::fromArray(array('hotel' => $hotel['Hotel']));
        $this->set(compact('xml')); 
	}

	private function getFreeRooms(){
		return $this->Hotel->Room->find('count');
	}
}
