<?php
App::uses('AppController', 'Controller');
/**
 * FlightPrices Controller
 *
 * @property FlightPrice $FlightPrice
 */
class FlightPricesController extends AppController
{

	/**
	 * index method
	 *
	 * @return void
	 */
	public function index()
	{
		$this->FlightPrice->recursive = 0;
		$flightPrices = $this->paginate();
		$this->set(compact('flightPrices'));
	}

	/**
	 * view method
	 *
	 * @param string $id
	 * @return void
	 */
	public function view($id = null)
	{
		$this->FlightPrice->id = $id;
		if (!$this->FlightPrice->exists()) {
		//	$this->Session->setFlash(__('Invalid flight price.'), 'Default/Flash/error');
			$this->redirect(array('action' => 'index'));
		}
		$flightPrice = $this->FlightPrice->read(null, $id);
		$this->set(compact('flightPrice'));
	}

	/**
	 * add method
	 *
	 * @return void
	 */
	public function add()
	{
		if ($this->request->is('post')) {
			$this->FlightPrice->create();
			if ($this->FlightPrice->save($this->request->data)) {
				$this->flash(__('Flightprice saved.'), array('action' => 'index'));
			} else {
			}
		}
		$flights = $this->FlightPrice->Flight->find('list');
		$classes = $this->FlightPrice->Class->find('list');
		$this->set(compact('flights', 'classes'));
	}

	/**
	 * edit method
	 *
	 * @param string $id
	 * @return void
	 */
	public function edit($id = null)
	{
		$this->FlightPrice->id = $id;
		if (!$this->FlightPrice->exists()) {
		//	$this->Session->setFlash(__('Invalid flight price.'), 'Default/Flash/error');
			$this->redirect(array('action' => 'index'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->FlightPrice->save($this->request->data)) {
				$this->flash(__('The flight price has been saved.'), array('action' => 'index'));
			} else {
			}
		} else {
			$this->request->data = $this->FlightPrice->read(null, $id);
		}
		$flights = $this->FlightPrice->Flight->find('list');
		$classes = $this->FlightPrice->Class->find('list');
		$this->set(compact('flights', 'classes'));
	}

	/**
	 * delete method
	 *
	 * @throws MethodNotAllowedException
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null)
	{
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->FlightPrice->id = $id;
		if (!$this->FlightPrice->exists()) {
		//	$this->Session->setFlash(__('Invalid Flight price'), 'Default/Flash/error');
			$this->redirect(array('action' => 'index'));
		}
		if ($this->FlightPrice->delete()) {
			$this->flash(__('Flight price deleted'), array('action' => 'index'));
		}
		$this->flash(__('Flight price was not deleted'), array('action' => 'index'));
		$this->redirect(array('action' => 'index'));
	}
}
