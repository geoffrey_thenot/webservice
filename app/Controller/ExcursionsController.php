<?php
App::uses('AppController', 'Controller');
/**
 * Excursions Controller
 *
 * @property Excursion $Excursion
 */
class ExcursionsController extends AppController
{

	/**
	 * index method
	 *
	 * @return void
	 */
	public function index()
	{
		$this->Excursion->recursive = 0;
		$excursions = $this->paginate();
		$this->set(compact('excursions'));
	}

	/**
	 * view method
	 *
	 * @param string $id
	 * @return void
	 */
	public function view($id = null)
	{
		$this->Excursion->id = $id;
		if (!$this->Excursion->exists()) {
			//$this->Session->setFlash(__('Invalid excursion.'), 'Default/Flash/error');
			$this->redirect(array('action' => 'index'));
		}
		$excursion = $this->Excursion->read(null, $id);
		$this->set(compact('excursion'));
	}

	/**
	 * add method
	 *
	 * @return void
	 */
	public function add()
	{
		if ($this->request->is('post')) {
			$this->Excursion->create();
			if ($this->Excursion->save($this->request->data)) {
			//	$this->Session->setFlash(__('The excursion has been saved'), 'Default/Flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
			//	$this->Session->setFlash(__('The excursion could not be saved. Please, try again.', 'Default/Flash/error'));
				$this->redirect($this->referer());
			}
		}
	}

	/**
	 * edit method
	 *
	 * @param string $id
	 * @return void
	 */
	public function edit($id = null)
	{
		$this->Excursion->id = $id;
		if (!$this->Excursion->exists()) {
		//	$this->Session->setFlash(__('Invalid excursion.'), 'Default/Flash/error');
			$this->redirect(array('action' => 'index'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Excursion->save($this->request->data)) {
			//	$this->Session->setFlash(__('The excursion has been updated.'), 'Default/Flash/information');
				$this->redirect(array('action' => 'index'));
			} else {
			//	$this->Session->setFlash(__('The excursion could not be saved. Please, try again.', 'Default/Flash/error'));
				$this->redirect($this->referer());
			}
		} else {
			$this->request->data = $this->Excursion->read(null, $id);
		}
	}

	/**
	 * delete method
	 *
	 * @throws MethodNotAllowedException
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null)
	{
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Excursion->id = $id;
		if (!$this->Excursion->exists()) {
			//$this->Session->setFlash(__('Invalid Excursion'), 'Default/Flash/error');
			$this->redirect(array('action' => 'index'));
		}
		if ($this->Excursion->delete()) {
		//	$this->Session->setFlash(__('Excursion deleted'), 'Default/Flash/default');
			$this->redirect(array('action' => 'index'));
		}
		//$this->Session->setFlash(__('Excursion was not deleted'), 'Default/Flash/error');
		$this->redirect(array('action' => 'index'));
	}

	public function listing(){
		$excursions = $this->Excursion->find("all");

		$_excursions = array('excursions' => null);
        foreach ($excursions as $excursion) {
                $_excursions['excursions']['excursion'][] = $excursion['Excursion'];
        }
        $xml = Xml::fromArray($_excursions);
        $this->set(compact('xml'));
	}
}
