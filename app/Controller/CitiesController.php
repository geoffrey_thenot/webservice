<?php
App::uses('AppController', 'Controller');
/**
 * Cities Controller
 *
 * @property City $City
 */
class CitiesController extends AppController
{
	/**
	 * index method
	 *
	 * @return void
	 */
	public function index()
	{
		$this->City->recursive = 0;
		$cities = $this->paginate();
		$this->set(compact('cities'));
	}

	/**
	 * view method
	 *
	 * @param string $id
	 * @return void
	 */
	public function view($id = null)
	{
		$this->City->id = $id;
		if (!$this->City->exists()) {
		//	$this->Session->setFlash(__('Invalid city.'), 'Default/Flash/error');
			$this->redirect(array('action' => 'index'));
		}
		$city = $this->City->read(null, $id);
		$this->set(compact('city'));
	}

	/**
	 * add method
	 *
	 * @return void
	 */
	public function add()
	{
		if ($this->request->is('post')) {
			$this->City->create();
			if ($this->City->save($this->request->data)) {
			//	$this->Session->setFlash(__('The city has been saved'), 'Default/Flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
			//	$this->Session->setFlash(__('The city could not be saved. Please, try again.', 'Default/Flash/error'));
				$this->redirect($this->referer());
			}
		}
		$countries = $this->City->Country->find('list');
		$this->set(compact('countries'));
	}

	/**
	 * edit method
	 *
	 * @param string $id
	 * @return void
	 */
	public function edit($id = null)
	{
		$this->City->id = $id;
		if (!$this->City->exists()) {
			//$this->Session->setFlash(__('Invalid city.'), 'Default/Flash/error');
			$this->redirect(array('action' => 'index'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->City->save($this->request->data)) {
			//	$this->Session->setFlash(__('The city has been updated.'), 'Default/Flash/information');
				$this->redirect(array('action' => 'index'));
			} else {
			//	$this->Session->setFlash(__('The city could not be saved. Please, try again.', 'Default/Flash/error'));
				$this->redirect($this->referer());
			}
		} else {
			$this->request->data = $this->City->read(null, $id);
		}
		$countries = $this->City->Country->find('list');
		$this->set(compact('countries'));
	}

	/**
	 * delete method
	 *
	 * @throws MethodNotAllowedException
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null)
	{
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->City->id = $id;
		if (!$this->City->exists()) {
			//$this->Session->setFlash(__('Invalid City'), 'Default/Flash/error');
			$this->redirect(array('action' => 'index'));
		}
		if ($this->City->delete()) {
			//$this->Session->setFlash(__('City deleted'), 'Default/Flash/default');
			$this->redirect(array('action' => 'index'));
		}
		//$this->Session->setFlash(__('City was not deleted'), 'Default/Flash/error');
		$this->redirect(array('action' => 'index'));
	}

	public function listing(){
		$conditions = array();

		if (!empty($this->request['url']['country'])) {
			$conditions['Country.name'] = $this->request['url']['country'];
		}

		if (!empty($this->request['url']['city'])) {

			//On récupère l'id de la ville
			$conditions['City.id'] = $this->request['url']['city'];

			$this->City->id = $conditions['City.id'];

			$city = $this->City->read(null, $conditions['City.id']);

			$xml = Xml::fromArray(array('city' => $city['City']));
        	$this->set(compact('xml'));
		}
		else
		{
			$cities = $this->City->find("all", array(
											'conditions' => $conditions));

        	$_cities = array('cities' => null);

        	foreach ($cities as $city) {
                $_cities['cities']['city'][] = $city['City'];
        	}        

        	$xml = Xml::fromArray($_cities);
        	$this->set(compact('xml'));
		}        
	}
}
